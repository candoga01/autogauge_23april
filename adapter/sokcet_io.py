import asyncio
import json
import logging
from threading import Thread
from tkinter import Toplevel, GROOVE, RIGHT
from  socketio import exceptions
import socketio
import bind
from models.workspace_Model import *


class socketServer_session(Thread):
    def __init__(self,server="192.168.1.212",port=12450,*args, **kwargs):
        super().__init__()
        self.setDaemon(True)
        self.name = "data [Web-Socket] service"
        self.loop = asyncio.new_event_loop()
        self.loop.set_debug(False)
        self.mainLOOP = kwargs['loop']
        self.bus = kwargs['bus']
        self.refresh_required=[0,False]
        self.scoket_url = f"http://{server}:{port}"

    def run(self):
        try:
            try:
                self.loop.run_until_complete(self.TASK())
            except socketio.exceptions.ConnectionError as er:
                logging.info(er)
        except Exception as er:
            logging.error(f"[err ]=> {er}", exc_info=True)
        finally:
            self.loop.close()

    async def TASK(self):
        try:
            self.sio = socketio.AsyncClient()
            await self.call_backs()
            await self.sio.connect(self.scoket_url)
            await self.sio.wait()

        except socketio.exceptions.ConnectionError:
            logging.error("Failed To Connect Sokcet.IO Server")
            await asyncio.sleep(1)
            CHILD = bind.get_all_children_from_widget(self.bus['workspace'])
            from PIL import ImageTk,Image
            img = Image.open("./img/accesspoint.png")
            new_image = img.resize((300, 300))
            render = ImageTk.PhotoImage(new_image)
            CHILD[0].config(text=f"Starting... AccessPoint/Hotspot", font=("Arial",15,'bold'),fg='blue',image=render,compound=LEFT)
            CHILD[0].image=render
            self.bus['station_data'] = None
            from api.Access_Point import AccessPoint
            local_access_point = AccessPoint()
            local_access_point.start_AccessPoint()
            CHILD = bind.get_all_children_from_widget(self.bus['workspace'])
            for I in CHILD: I.destroy()
            self.bus['workspace'].update()
            parent = self.bus['workspace']
            await No_Server_Connectivity_Display().init_NoNetwork(parent,self.bus,asyncloop=self.mainLOOP,local_access_point=local_access_point,fill=BOTH,expand=1,padx=50,pady=50)

#**********************************************************
    async def wait_for_footer_load(self,keys='footers'):
        while (1):
            if (keys in self.bus):
                break
        return self.bus[keys]
#**********************************************************
    async def call_backs(self):
        @self.sio.event
        async def connect():
            await self.sio.emit("station_information", {"ID": bind.get_station_id()})
            await self.sio.emit("get_station_config_data_list", {"station_id": bind.get_station_id()})
            FOOTER = await self.wait_for_footer_load(keys='footers')
            FOOTER[3].config(text="Socket : Online",fg='green',font=("Arial",13,"bold"))
            SESSION_INFO = await self.wait_for_footer_load(keys='session_info')
            SESSION_INFO.pack(side=RIGHT,padx=5)

        @self.sio.on("token_id")
        async def my_token_id(data):
            SESSION_INFO = await self.wait_for_footer_load(keys='session_info')
            logging.info(SESSION_INFO)

        @self.sio.on('station_data_from_db')
        async def on_station_data_received_From_Database(data):
            if(data['status'] and data['mode']==0):
                await asyncio.sleep(0.2)
                workspace = self.bus['workspace']
                self.bus['station_data'] = data['data']['data']
                if(self.refresh_required[1]==False and self.refresh_required[0]==0):
                    await Station_Data_Display().init_DISPLAY(workspace,self.bus,fill=BOTH,expand=1)
                    self.refresh_required = [1,True]

        @self.sio.event
        async def disconnect():
            FOOTER = await self.wait_for_footer_load(keys='footers')
            FOOTER[3].config(text="Socket : Offline", fg='red', font=("Arial", 13, "bold"))
            SESSION_INFO = await self.wait_for_footer_load(keys='session_info')
            SESSION_INFO.pack_forget()

