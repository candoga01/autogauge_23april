import getpass
import json
import subprocess
import asyncio,logging
from threading import Thread
from tkinter import BOTH, Label

PORT_NUMBER= 12450

from simple_websocket_server import WebSocketServer, WebSocket
clients = []
global temp_flg,MODE_CNT
global BUS_ADAPTER,DYNAMICrunning_frame
class SimpleChat(WebSocket):
    global BUS_ADAPTER,temp_flg,MODE_CNT,DYNAMICrunning_frame
    def handle(self):
        global MODE_CNT,temp_flg,DYNAMICrunning_frame
        receive_data = self.data
        OBJ = json.loads(receive_data)
        if('clone_device_info' in OBJ):
            if(temp_flg):
                run = BUS_ADAPTER['runningfrm']
                run[0].pack_forget()
                run[2].destroy()
                run[1].update()
                from models.workspace_Model import CLONE_PHONE_INFO
                DYNAMICrunning_frame = CLONE_PHONE_INFO().Access_Device_Phone_Info(run[1],info={"phone":OBJ['clone_device_info']},fill=BOTH,expand=1,padx=50,pady=50)
                temp_flg = False
                MODE_CNT = 10

    def connected(self):
        global temp_flg
        logging.info(f"{self.address} connected")
        if temp_flg ==False:
            run = BUS_ADAPTER['runningfrm']
            run[0].pack_forget()
            run[1].update()
            XC  =Label(run[1],text='Connected getting Device Information...')
            XC.pack(expand=1)
            run[2] = XC
        for client in clients:
            client.send_message(self.address[0] + u' - connected')
        clients.append(self)
        temp_flg = True



    def handle_close(self):
        global MODE_CNT,DYNAMICrunning_frame,temp_flg
        clients.remove(self)
        logging.info(f"{self.address} Disconnected")
        logging.info(f"VALUE = {MODE_CNT}    flag={temp_flg}")
        if MODE_CNT == 10 and temp_flg==True:
            run = BUS_ADAPTER['runningfrm']
            run[2].destroy()
            DYNAMICrunning_frame.destroy()
            run[0].pack(fill=BOTH,expand=1,padx=50,pady=50)
            run[1].update()
            MODE_CNT = 0

class PHONE_CONNECTIVITY_LOCAL_SERVER(Thread):
    def __init__(self,server="0.0.0.0",port=PORT_NUMBER,*args, **kwargs):
        super().__init__()
        global BUS_ADAPTER,temp_flg,MODE_CNT
        BUS_ADAPTER = {}
        temp_flg = False
        MODE_CNT= 0
        self.setDaemon(True)
        self.name = "ANDROID_ENV_SERVER [Web-Socket] service"
        self.loop = asyncio.new_event_loop()
        self.loop.set_debug(False)
        self.mainLOOP = kwargs['loop']
        bus = kwargs['bus']
        running_frame = kwargs['current_frame']
        self.scoketCONF = [server,port]
        BUS_ADAPTER = {"bus":bus,"runningfrm":running_frame}

    def run(self):
        try:
            self.loop.run_until_complete(self.TASK())
        except Exception as er:
            logging.error(f"[err ]=> {er}", exc_info=True)
        finally:
            self.loop.close()

    async def TASK(self):
        server = WebSocketServer('0.0.0.0', PORT_NUMBER, SimpleChat)
        server.serve_forever()

    async def stop_Server(self):
        res = subprocess.getoutput(f"lsof -i:{PORT_NUMBER}").splitlines()
        for index,I in enumerate(res):
            if(index >=1):
                pid = I.split(getpass.getuser())[0].split(" ")[1].replace(" ","")
                subprocess.getoutput(f"kill -9 {pid}")