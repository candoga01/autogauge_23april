import threading,logging
from tkinter import *
import asyncio,logging
# App Windows
from adapter.sokcet_io import socketServer_session
from models.Layout import *
from models.async_roottkinter.saab_tkinetr_run import TkEventLoop

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

class Window(Tk):
    def __init__(self,bus={},*args,**kwargs):
        super().__init__()
        self.BUS = bus
        self.options_kwargs = kwargs
        W,H = self.winfo_screenwidth(),self.winfo_screenheight()
        self.geometry(f"{W-200}x{H-200}+{100}+{100}")
        self.resizable(False,False)
        self.BUS['root'] = self
        if('fullscreen' in kwargs.keys()):
            if(kwargs['fullscreen']):
                self.geometry(f"{W}x{H}+{0}+{0}")
        if('overrideredirect' in kwargs.keys()):
            if(kwargs['overrideredirect']):
                self.overrideredirect(kwargs['overrideredirect'])
                self.config(bd=3,relief=GROOVE)
        if('title' in kwargs.keys()):
            self.title(kwargs['title'])
            self.BUS['title'] = kwargs['title']
        def close_application():
            AccessPoint().stop_AccessPoint()
            self.quit()
        self.protocol("WM_DELETE_WINDOW",lambda:close_application())
#=================================================================================
        def start_task_in_background_loading():
            async def run_task():
                if(kwargs['overrideredirect']):
                    HEADER = await Header_().init_header(bus=self.BUS,fill=X,side=TOP)
                FOOTER = await Footer_().init_footer(bus=self.BUS,fill=X,side=BOTTOM)
                WORKSPACE = await WORKSPACE_().init_workspace(bus=self.BUS,fill=BOTH,expand=1)
                Label(WORKSPACE, text='Processing..').pack(expand=1)
                self.BUS['workspace'] = WORKSPACE
            loop2 = asyncio.new_event_loop()
            try:
                loop2.run_until_complete(run_task())
            except :pass
            finally:
                loop2.close()

#=====================================================================================
        # load ui graphics into background task
        t1 = threading.Thread(target=start_task_in_background_loading,args=())
        t1.start()

    @asyncio.coroutine
    def run(self):
        yield from asyncio.sleep(0.001)
        try:
            yield from TkEventLoop(self).mainloop()
        except KeyboardInterrupt as er:
            pass
        except KeyError as er:
            pass

# create async io future task/ startup init task
def future_task():
    BUS = {}
    app = Window(bus=BUS,title="Auto-Offset System (saab engineering)",fullscreen=False,overrideredirect=False)
    socketServer_session(bus=BUS,loop=loop).start()
    return app

# runnabele main function
if __name__ == '__main__':
    # define asyncronous event loop***
    logging.info("started Auto-Offset System")
    loop = asyncio.get_event_loop()
    try:
        # call task before start loop
        app = future_task()
        # start loop until application is running (run time)
        loop.run_until_complete(app.run())
    except:pass
    finally:
        # at last close loop & shut done enviroment ( Apps & Services )
        loop.close()
        logging.info("Stop Auto-Offset System")