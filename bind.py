import netifaces

def get_station_id():
    iface  = netifaces.interfaces()
    for i in iface:
        if(str(i).lower()[0].__contains__("e")):
            return str(netifaces.ifaddresses(i)[netifaces.AF_LINK][0]['addr']).upper().replace(":","")


def get_ip_Address_for_access_point():
    iface = netifaces.interfaces()
    for i in iface:
        if(i[0].lower().__eq__("w")):
            ipAddr = netifaces.ifaddresses(i)[netifaces.AF_INET][0]['addr']
            netmask = netifaces.ifaddresses(i)[netifaces.AF_INET][0]['netmask']
            bradcast = netifaces.ifaddresses(i)[netifaces.AF_INET][0]['broadcast']
            # print(f"{netifaces.AF_INET}  {netifaces.AF_INET6}  {netifaces.AF_LINK}")
            return {"ip":ipAddr,"mask":netmask,"bradcast":bradcast}
    return {}

def get_all_children_from_widget (wid) :
    _list = wid.winfo_children()
    for item in _list :
        if item.winfo_children() :
            _list.extend(item.winfo_children())
    return _list
