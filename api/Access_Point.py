import subprocess as sp
import bind
class AccessPoint():
    def __init__(self,ssid=f"autoGauge_SAAB_{bind.get_station_id()}",pwd="saab123"):
        self.profilename = f"autoGauge_SAAB_{bind.get_station_id()}"
        self.ssid_ = ssid
        self.pwd_ = pwd
        # Create a connection
        cmd1 = f"nmcli connection add type wifi ifname '*' con-name {self.profilename} autoconnect no ssid {ssid}"
        # Put it in Access Point
        cmd2 = f"nmcli connection modify {self.profilename} 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared"
        # Set a WPA password (you should change it)
        cmd3 = f"nmcli connection modify {self.profilename} 802-11-wireless-security.key-mgmt wpa-psk 802-11-wireless-security.psk {pwd}"
        # Enable it (run this command each time you want to enable the access point)
        self.start_ap = f"nmcli connection up {self.profilename}"
        # Disable it (run this command each time you want to enable the access point)
        self.stop_ap = f"nmcli connection down {self.profilename}"

        std0 = sp.getoutput(f"nmcli connection show {ssid}")
        if(std0.split("-")[1].lower().__contains__("no such connection profile")):
            add_profile = sp.getoutput(cmd1)
        createAP = sp.getoutput(cmd2)
        set_Password = sp.getoutput(cmd3)

    def start_AccessPoint(self):
        start_AP = sp.getoutput(self.start_ap)
        if(start_AP.lower().__contains__("connection successfully activated")):
            return True
        return False

    def stop_AccessPoint(self):
        stop_AP = sp.getoutput(self.stop_ap)
        if (stop_AP.lower().__contains__(f"connection '{self.profilename.lower()}' successfully deactivated")):
            return True
        return False