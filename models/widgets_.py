from tkinter import *
from tkinter import ttk


class verticalSCROLL_Table(Frame):
    def __init__(self,Parent,VS=True,HS=False,COLUMN_NAME=[],COLUMN_SIZE=[],*args,**kwargs):
        Frame.__init__(self,master=Parent)
        self._HEADER = COLUMN_NAME
        tree = ttk.Treeview(self, columns=self._HEADER, cursor="hand2", selectmode="browse",height=7)
        if(kwargs):
            tree.config(kwargs)
        if(HS):
            scrollbarx = ttk.Scrollbar(self, orient=HORIZONTAL)
            scrollbarx.config(command=tree.xview)
            scrollbarx.pack(side=BOTTOM, fill=X)
            tree.configure(xscrollcommand=scrollbarx.set)

        if(VS):
            vsb = ttk.Scrollbar(self, orient="vertical", command=tree.yview)
            vsb.pack(side=RIGHT, fill=Y, pady=10)
            self.treeScroll = vsb
            tree.configure(yscrollcommand=vsb.set)
        tree.pack(side=LEFT, fill=BOTH, pady=10, padx=(10, 0))
        tree["columns"] = (COLUMN_NAME)
        tree['show'] = 'headings'
        for index,i in enumerate(COLUMN_NAME):
            tree.column(COLUMN_NAME[index], width=COLUMN_SIZE[index], anchor='c')
            tree.heading(COLUMN_NAME[index], text=str(i))
        self._TREE = tree

    def insert_data_info_table(self,DATA):
        if (len(DATA) >= len(self._HEADER)):
            str_data = ''.join(DATA).replace("NC mm"," ")
            _DATA = []
            for index,i in enumerate(self._HEADER):
                if(index==len(self._HEADER)-1):
                    _DATA.append(str_data)
                else:
                    _DATA.append("Empty Data")
            self._TREE.insert('', 'end', tag="center", text="✔", values=(_DATA))


class scrolling_frame(Frame):
    def __init__(self,parent,COLOR_='gray',VS=True,HS=True,cursor='hand2',Frm_design={},**kwargs):
        Frame.__init__(self,parent,bg=COLOR_)
        self.canvas = Canvas(self,bg=COLOR_)
        self.config(cursor='hand2')
        if(Frm_design):
            self.config(Frm_design)
        if(VS):
            self.Vertical_scrollbar = ttk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        if(HS):
            self.Horizontal_scrollbar = ttk.Scrollbar(parent,orient="horizontal",command=self.canvas.xview)
        self.scrollable_frame = Frame(self.canvas,bg=COLOR_)
        self.scrollable_frame.bind(
            "<Configure>",
            lambda e: self.canvas.configure(
                scrollregion=self.canvas.bbox("all")
            )
        )
        self.canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")
        if(VS==True and HS==True):
            self.canvas.configure(yscrollcommand=self.Vertical_scrollbar.set,xscrollcommand=self.Horizontal_scrollbar.set)
        elif (VS==True and HS==False):
            self.canvas.configure(yscrollcommand=self.Vertical_scrollbar.set)
        elif (VS == False and HS == True):
            self.canvas.configure(xscrollcommand=self.Horizontal_scrollbar.set)
        self.canvas.pack(side="left", fill="both", expand=True)
        if(VS):
            self.Vertical_scrollbar.pack(side="right", fill="y",padx=0,pady=10)
        if(HS):
            self.Horizontal_scrollbar.pack(side="bottom",fill=X)
        if(kwargs):
            try:
                self.pack(kwargs)
            except RuntimeError as er:
                pass