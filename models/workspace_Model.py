import asyncio
import json
import logging
import threading
from tkinter import *
from tkinter import ttk
import qrcode
import bind
from adapter.socketPHONE_server import PHONE_CONNECTIVITY_LOCAL_SERVER, PORT_NUMBER
from models.widgets_ import verticalSCROLL_Table

class Station_Data_Display(Frame):
    def __await__(self, *args, **kwargs):
        return self._async_init(*args, **kwargs).__await__()
    async def _async_init(self, *args, **kwargs):
        return self

    async def init_DISPLAY(self,parent,bus,*args,**kwargs):
        super().__init__(parent)
        data = bus['station_data']
        await asyncio.sleep(1)
        CHILD = bind.get_all_children_from_widget(parent)
        if(len(CHILD)==2):
            CHILD[0].destroy()
        parent.update()
        #===============================================================
        Lb1 = Listbox(self,cursor='hand2',bg=self.cget("bg"),relief=FLAT)
        await asyncio.sleep(0.2)
        default_part = None
        default_part = data[0]['partNumber']
        for index,I in enumerate(data):
            await asyncio.sleep(0.01)
            Lb1.insert(index+1, I['partNumber'])

        Lb1.pack(side=LEFT,fill=Y,padx=10,pady=10)
        def OnDouble(event):
            widget = event.widget
            selection = widget.curselection()
            value = widget.get(selection[0])
            bus['footers'][2].config(text=f"Part : {value}",fg='red')
            header.config(text=f'/ Inprocess-inspection / {value} ',font=("Arial",13,"bold"))
            tree.delete(*tree.get_children())
            for I in data:
                if(I['partNumber']==value):
                    prm = I['Parameter_OBJ']
                    for K,V in json.loads(prm).items():
                        arr = [K,V[0],V[1],V[2],V[4]]
                        tree.insert('', 'end', tag="center", text="✔", values=(arr))
        Lb1.bind("<Double-Button-1>", lambda event:OnDouble(event))
        ttk.Separator(self, orient=VERTICAL).pack(side=LEFT, fill=Y)
        frm = Frame(self,bg='gray')
        header = Label(frm,text='???',bg='yellow',fg='black',anchor='w',font=("Arial",13,"bold"))
        header.pack(side=TOP,fill=X,padx=2)
        tmp = Frame(frm)
        CL_N=['parms_name','Specification','Axis','Tool','#Nav']
        CL_S=[250,150,50,50,80]
        table = verticalSCROLL_Table(tmp,VS=True,HS=False,COLUMN_NAME=CL_N,COLUMN_SIZE=CL_S)
        table.pack(side=TOP,expand=1,anchor='nw')
        tree = table._TREE
        header.config(text=f'/ Inprocess-inspection / {default_part} ', font=("Arial", 13, "bold"))
        Lb1.select_set(0)
        for I in data:
            if (I['partNumber'] == default_part):
                prm = I['Parameter_OBJ']
                for K, V in json.loads(prm).items():
                    arr = [K, V[0], V[1], V[2], V[4]]
                    tree.insert('', 'end', tag="center", text="✔", values=(arr))
        bus['footers'][2].config(text=f"Part : {default_part}", fg='red')
        tmp.pack(fill=X,padx=(10,100),pady=(5))
        fr1 = Frame(frm,height=200)
        Label(fr1,text='No Machine Config Applyed Yet').pack(expand=1)
        fr1.pack(fill=X,padx=(10,100),pady=(20))

        btnContainer = Frame(frm,bg=frm.cget("bg"))
        btn1 = ttk.Button(btnContainer,text='Start-Inspection',cursor='hand2')
        btn1.pack(side=RIGHT,padx=10)
        btnContainer.pack(side=BOTTOM,fill=X,padx=10,pady=5)
        ttk.Separator(frm,orient=HORIZONTAL).pack(side=BOTTOM,fill=X)
        frm.pack(side=RIGHT,fill=BOTH,expand=1,pady=0)
        #===============================================================
        if(kwargs):
            self.pack(kwargs)
        return self


from PIL import ImageTk,Image
class No_Server_Connectivity_Display(Frame):
    def __await__(self, *args, **kwargs):
        return self._async_init(*args, **kwargs).__await__()
    async def _async_init(self, *args, **kwargs):
        return self

    async def init_NoNetwork(self,parent,bus,asyncloop=None,local_access_point=None,*args,**kwargs):
        super().__init__(parent)
        Label(self,text = "No Network Connectivity (for more details & Control Connect QR-)",bg='red',fg='white'
              ,font=("Arial",13,'bold')).\
            pack(side=TOP,fill=X)

        img = Image.open("./img/accesspoint.png")
        new_image = img.resize((30, 30))
        render = ImageTk.PhotoImage(new_image)
        LX = Label(self, text=f"  HotSport/Accesspoint Is Running...   '{local_access_point.profilename}'",
                   bg='yellow', fg='black',image=render,compound=LEFT
              , font=("Arial", 12, 'bold'))
        LX.image=render
        LX.pack(side=TOP, fill=X)
        # {'ip': '10.42.0.1', 'mask': '255.255.255.0', 'bradcast': '10.42.0.255'}
        SERVRE = PHONE_CONNECTIVITY_LOCAL_SERVER(loop=asyncloop,bus= bus,current_frame=[self,parent,None])
        SERVRE.start()
        pass_model = json.dumps({
          "networking":bind.get_ip_Address_for_access_point(),
          "connectivity":[local_access_point.ssid_,local_access_point.pwd_],
          "data_mgr":"nA",
          "SCOKET_DATA":{"apiKey":"@#1256SD5%*956@@Kl",
                         "socket_io":{"port":PORT_NUMBER,"ipaddress":bind.get_ip_Address_for_access_point()['ip'],"server":"runining.."}}
        })
        img = qrcode.make(pass_model)
        img.save('temp.png')
        img = Image.open("temp.png")
        new_image = img.resize((300,300))
        render = ImageTk.PhotoImage(new_image)
        imgS = Label(self, image=render, relief=FLAT, bd=0,bg='white')
        imgS.image = render
        imgS.pack(side=LEFT,fill=Y)
        #================================================
        tx = Frame(self)
        CL_N = ['Device-Name', 'Connection-Type','Paired','Device-ID','Mode']
        CL_S = [250, 150, 100, 150, 100]
        table = verticalSCROLL_Table(tx, VS=True, HS=False, COLUMN_NAME=CL_N, COLUMN_SIZE=CL_S)
        table.pack(side=TOP, expand=1, anchor='nw')
        tree = table._TREE
        tx.pack(side=RIGHT,fill=BOTH,expand=1)

        def reload():
            tree.delete(*tree.get_children())
        #***************************************************************
        tx2 = Frame(tx)
        btn1 = Button(tx2,text='Refresh',cursor='hand2',)
        tx2.pack(side=BOTTOM,fill=X)
        #================================================
        if(kwargs):
            self.pack(kwargs)
        return self



from PIL import ImageTk,Image
class CLONE_PHONE_INFO(Frame):
    def Access_Device_Phone_Info(self,parent,info={},*args,**kwargs):
        super().__init__(parent)
        img = Image.open("./img/PHONE.png")
        new_image = img.resize((400,400))
        render = ImageTk.PhotoImage(new_image)
        imgS = Label(self, image=render, relief=FLAT, bd=0)
        imgS.image = render
        imgS.pack(side=TOP,expand=1,fill=X)
        Label(self,text=info['phone'],font=("Arial",20,"bold"),fg='red').pack(fill=X,expand=1,pady=10)
        if(kwargs):
            self.pack(kwargs)
        return self