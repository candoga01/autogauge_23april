import asyncio
import os
from tkinter import *
from tkinter import ttk
from tkinter.font import Font
import bind
from api.Access_Point import AccessPoint


class Header_(Frame):
    def __await__(self, *args, **kwargs):
        return self._async_init(*args, **kwargs).__await__()
    async def _async_init(self, *args, **kwargs):
        super(Header_, self)._async_init()
        return self

    async def session_control_info_views(self,bus,*args,**kwargs):
        ttk.Separator(self, orient=VERTICAL).pack(side=RIGHT,fill=Y, pady=1,padx=(0,20))
        from PIL import ImageTk,Image
        img_ = ImageTk.PhotoImage(Image.open("./img/INFO.png").resize((20, 20)))
        session_ = Label(self,text=' Session_Details',image=img_,compound=LEFT,cursor='hand2')
        session_.image = img_
        f = Font(session_, session_.cget("font"))
        f.configure(underline=True)
        session_.configure(font=f)
        bus['session_info'] = session_

    async def init_header(self,bus,*args,**kwargs):
        self.title_txt = Label(self,text=bus['title'])
        self.title_txt.pack(side=LEFT,fill=X,padx=10)
        cls = Label(self,text=" X ",cursor='hand2',bg='red',fg='white',font=("Arial",13,'bold'))
        def close_application(event):
            AccessPoint().stop_AccessPoint()
            bus['root'].quit()
        cls.bind("<Button-1>",lambda event:close_application(event))
        cls.pack(side=RIGHT,padx=2)
        if(kwargs):
            self.pack(kwargs)
        await self.session_control_info_views(bus)
        ttk.Separator(master=bus['root'],orient=HORIZONTAL).pack(side=TOP,fill=X,padx=10)
        return self


class Footer_(Frame):
    def __await__(self, *args, **kwargs):
        return self._async_init(*args, **kwargs).__await__()
    async def _async_init(self, *args, **kwargs):
        super(Footer_, self)._async_init()
        return self
    async def init_footer(self,bus,*args,**kwargs):
        async def add_Lable(type='l',TEXT=None,color="black",*args2,**kwargs2):
            if(TEXT !=None):
                L1 = Label(self,text=TEXT)
            else:
                L1 = Label(self, text='???')
            if(type=='l'):
                L1.pack(side=LEFT,padx=2,fill=X)
            L1.config(fg=color)
            if(kwargs2):
                L1.config(kwargs2)
            if (type == 'r'):
                L1.config(width=20)
                L1.pack(side=RIGHT, padx=2)
                ttk.Separator(self,orient=VERTICAL).pack(side=RIGHT,padx=1,fill=Y,pady=1)
            return L1
        # =========================
        l1 = await add_Lable(type='l',TEXT=f"{os.getcwd()}",color='purple')
        l2 = await add_Lable(type='r',TEXT=f"ID : {bind.get_station_id()}")
        l3 = await add_Lable(type='r',TEXT=f"Socket :: Offline", color='red')
        l4 = await add_Lable(type='r',TEXT=f"Part : no info?",color='Gray')
        l5 = await add_Lable(type='r',TEXT=f"[ ToolBar ]",color='blue',cursor='hand2',font=("Arial",13,"bold"))
        bus['footers'] = [l1,l5,l4,l3,l2]
        # =========================
        if(kwargs):
            self.pack(kwargs)
        ttk.Separator(master=bus['root'],orient=HORIZONTAL).pack(side=BOTTOM,fill=X,padx=10)
        return self

class WORKSPACE_(Frame):
    def __await__(self, *args, **kwargs):
        return self._async_init(*args, **kwargs).__await__()
    async def _async_init(self, *args, **kwargs):
        super(WORKSPACE_, self)._async_init()
        return self
    async def init_workspace(self,bus,*args,**kwargs):
        if(kwargs):
            self.pack(kwargs)
        return self