import time

import socketio

sio = socketio.Client()


@sio.event
def connect():
    print('connection established')
    while 1:
        time.sleep(1)
        string = input("Enter Message : ")
        sio.emit('msg',string)


@sio.event
def my_message(data):
    print('message received with ', data)

@sio.event
def disconnect():
    print('disconnected from server')


sio.connect('http://localhost:2301')
sio.wait()